# ProjectsCpp
A variety of object oriented programming tasks asigned by my coding teacher and implemented by me and/or co-students. 
For educational purposes. 
Credits to my teacher George Meletiou at UNIWA Greece.

For every task there is an explanation and the code.
You can freely do your own implementation of the task and compare to ours.
All the uploaded code is error-free (using gcc/g++ v.5.5.0) and tested with targeted examples for logical errors. Also most of the time it is supervised by our teacher.
Feel free to reach back with any suggestions or questions concerning any of the given tasks (or maybe new ones)!

The final release version of the projects can be found under the Master branch.
For the work repo and/or to contribute with your own project please checkout the dev branch.

Have a nice time coding!
Shane
